import 'package:flutter/material.dart';
import '../utils/auth.dart';

class HomePage extends StatelessWidget {

  const HomePage({this.auth, this.onSignedOut});
  
  final BaseAuth auth;
  final VoidCallback onSignedOut;

  void _signOut() async {
    try {
      await auth.signOut().then((result) {
        onSignedOut();
      });
      
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home page'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            tooltip: 'Sair',
            onPressed: _signOut,
          ),
        ],
      ),
      body: Container(
        child: Center(
          child: Text('Welcome', style: TextStyle(fontSize: 32.0),),
        ),
      ),
    );
  }
}