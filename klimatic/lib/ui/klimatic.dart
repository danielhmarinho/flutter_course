import 'package:flutter/material.dart';
import '../util/utils.dart' as util;
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Klimatic extends StatefulWidget {
  @override
  _KlimaticState createState() => _KlimaticState();
}

class _KlimaticState extends State<Klimatic> {

  String _cityEntered;

  Future _goToNextScreen(BuildContext context) async {
    Map results = await Navigator.of(context).push(
      MaterialPageRoute<Map>(builder: (BuildContext context) {
        return ChangeCity();
      })
    );
    if(results != null && results.containsKey('city')) {
      _cityEntered = results['city'];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Klimatic'),
        backgroundColor: Colors.redAccent,
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.location_city), 
            onPressed: () { _goToNextScreen(context); } 
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: Image.asset('images/umbrella.png',
              width: 490.0,
              height: 1200.0,
              fit: BoxFit.fill,
            ),
          ),
          Container(
            alignment: Alignment.topRight,
            margin: EdgeInsets.fromLTRB(0.0, 10.0, 20.0, 0.0),
            child: Text(
              '${_cityEntered == null ? util.defaultCity : _cityEntered}',
              style: cityStyle(),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Image.asset('images/light_rain.png'),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0.0, 250.0, 0.0, 0.0),
            alignment: Alignment.center,
            child: updateTempWidget(_cityEntered)
          ),
        ],
      ),
    );
  }
}

Future<Map> getWeather(String appId, String city) async {
  String apiUrl = '${util.weatherApiUrl}?q=$city&appid=${util.apiId}&units=metric';
  http.Response response = await http.get(apiUrl);
  return json.decode(response.body);
}

Widget updateTempWidget(String city) {
  return FutureBuilder(
    future: getWeather(util.apiId, city == null ? util.defaultCity : city),
    builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
      if(snapshot.hasData) {
        Map content = snapshot.data;
        return Container(
          margin: EdgeInsets.only(top: 100.0),
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text(
                  '${content['main']['temp'].toString()}°C',
                  style: TextStyle(
                    fontStyle: FontStyle.normal,
                    fontSize: 50.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.white
                  ),
                ),
                subtitle: ListTile(
                  title: Text(
                    'Umidade: ${content['main']['humidity'].toString()}%\n'
                    'Mínima: ${content['main']['temp_min'].toString()}°C\n'
                    'Máxima: ${content['main']['temp_max'].toString()}°C\n',
                    style: extraData(),
                  ),
                ),
              ),
            ],
          ),
        );
      } else {
        return Container();
      }
    }
  );
}

class ChangeCity extends StatelessWidget {

  final _cityFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Change city'),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: Image.asset(
              'images/white_snow.png',
              width: 490.0,
              height: 1200.0,
              fit: BoxFit.fill
            )
          ),
          ListView(
            children: <Widget>[
              ListTile(
                title: TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter city',
                  ),
                  controller: _cityFieldController,
                  keyboardType: TextInputType.text
                ),
              ),
              ListTile(
                title: FlatButton(
                  onPressed: () {
                    Navigator.pop(context, {
                      'city': _cityFieldController.text 
                    });
                  },
                  color: Colors.redAccent,
                  textColor: Colors.white70,
                  child: Text('Get Weather'),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

TextStyle extraData() {
  return TextStyle(
    color: Colors.white70,
    fontStyle: FontStyle.normal,
    fontSize: 18.0
  );
} 

TextStyle cityStyle() {
  return TextStyle(
    color: Colors.white,
    fontSize: 22.0,
    fontStyle: FontStyle.italic
  );
}

TextStyle tempStyle() {
  return TextStyle(
    color: Colors.white,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w500,
    fontSize: 50.0
  );
}